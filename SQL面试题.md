# 准备工作

已知有如下4张表：

学生表：student(学号,学生姓名,出生年月,性别)

成绩表：score(学号,课程号,成绩)

课程表：course(课程号,课程名称,教师号)

教师表：teacher(教师号,教师姓名)

![](http://imgcloud.duiyi.xyz//data20200625133948.png)

## 一、创建数据库和表

为了演示题目的运行过程，我们先按下面语句在客户端HeidiSQL中创建数据库和表。

![](http://imgcloud.duiyi.xyz//data20200625134225.png)

### 1、创建表

#### 1.1、创建学生表（student）

![](http://imgcloud.duiyi.xyz//data20200625134620.png)

#### 1.2、创建成绩表（score）

创建"成绩表“。“课程表的“学号”和“课程号”一起设置为主键约束（联合主键），“成绩”这一列设置为数值类型（float，浮点数值）

![](http://imgcloud.duiyi.xyz//data20200625134840.png)

#### 1.3、创建课程表（course）

课程表的“课程号”设置为主键约束

![](http://imgcloud.duiyi.xyz//data20200625134926.png)

#### 1.4、教师表（teacher）

教师表的“教师号”列设置为主键约束，

教师姓名这一列设置约束为“null”（红框的地方不勾选），表示这一列允许包含空值（null）

![](http://imgcloud.duiyi.xyz//data20200625135155.png)

### 2、向表中添加数据

![](http://imgcloud.duiyi.xyz//data20200625135236.png)

#### 2.1、学生表

```mysql
insert into student(学号,姓名,出生日期,性别) 
values('0001' , '猴子' , '1989-01-01' , '男');

insert into student(学号,姓名,出生日期,性别) 
values('0002' , '猴子' , '1990-12-21' , '女');

insert into student(学号,姓名,出生日期,性别) 
values('0003' , '马云' , '1991-12-21' , '男');

insert into student(学号,姓名,出生日期,性别) 
values('0004' , '王思聪' , '1990-05-20' , '男');
```

#### 2.2、成绩表（score）

```mysql
insert into score(学号,课程号,成绩) 
values('0001' , '0001' , 80);

insert into score(学号,课程号,成绩) 
values('0001' , '0002' , 90);

insert into score(学号,课程号,成绩) 
values('0001' , '0003' , 99);

insert into score(学号,课程号,成绩) 
values('0002' , '0002' , 60);

insert into score(学号,课程号,成绩) 
values('0002' , '0003' , 80);

insert into score(学号,课程号,成绩) 
values('0003' , '0001' , 80);

insert into score(学号,课程号,成绩) 
values('0003' , '0002' , 80);

insert into score(学号,课程号,成绩) 
values('0003' , '0003' , 80);
```

#### 2.3、课程表

```mysql
insert into course(课程号,课程名称,教师号)
values('0001' , '语文' , '0002');

insert into course(课程号,课程名称,教师号)
values('0002' , '数学' , '0001');

insert into course(课程号,课程名称,教师号)
values('0003' , '英语' , '0003');
```

#### 2.4、教师表

```mysql
insert into teacher(教师号,教师姓名) 
values('0001' , '孟扎扎');

insert into teacher(教师号,教师姓名) 
values('0002' , '马化腾');

-- 这里的教师姓名是空值（null）
insert into teacher(教师号,教师姓名) 
values('0003' , null);

-- 这里的教师姓名是空字符串（''）
insert into teacher(教师号,教师姓名) 
values('0004' , '');
```



# 50道面试题

为了方便学习，将50道面试题进行了分类

## 一、简单查询

![](http://imgcloud.duiyi.xyz//data20200625140637.png)

查询姓“猴”的学生名单

![](http://imgcloud.duiyi.xyz//data20200625140706.png)

### 1.查询姓“孟”老师的个数

```mysql
select count(教师号)
from teacher
where 教师姓名 like '孟%';
```



## 二、汇总分析

![](http://imgcloud.duiyi.xyz//data20200625140912.png)

### 2、查询课程编号为“0002”的总成绩

```mysql
select sum(成绩)
from score
where 课程号 = '0002';
```

### 3、查询选了课程的学生人数

```mysql
select count(distinct 学号) as 学生人数 
from score;
```

![](http://imgcloud.duiyi.xyz//data20200625142340.png)

### 4、查询各科成绩最高和最低的分， 以如下的形式显示：课程号，最高分，最低分

```mysql
select 课程号,max(成绩) as 最高分,min(成绩) as 最低分
from score
group by 课程号;
```

### 5、查询每门课程被选修的学生数

```mysql
select 课程号, count(学号) as 人数
from score
group by 课程号;
```

### 6、查询男生、女生人数

```mysql
select 性别,count(*) as 人数
from student
group by 性别;
```

![](http://imgcloud.duiyi.xyz//data20200625144136.png)

### 7、查询平均成绩大于60分学生的学号和平均成绩

```mysql
select 学号, avg(成绩) as 平均成绩
from score
group by 学号
having avg(成绩)>60;
```

### 8、查询至少选修两门课程的学生学号

```mysql
select 学号, count(课程号) as 选修课程数目
from score
group by 学号
having count(课程号)>=2;
```

### 9、查询同名同姓学生名单并统计同名人数

```mysql
select 姓名,count(*) as 人数
from student
group by 姓名
having count(*)>=2;
```

### 10、查询不及格的课程并按课程号从大到小排列

```mysql
select 课程号
from score 
where 成绩<60
order by 课程号 desc;
```

### 11、查询每门课程的平均成绩，结果按平均成绩升序排序，平均成绩相同时，按课程号降序排列

```mysql
select 课程号, avg(成绩) as 平均成绩
from score
group by 课程号
order by 平均成绩 asc,课程号 desc;
```

### 12、检索课程编号为“0004”且分数小于60的学生学号，结果按按分数降序排列

```mysql
select 学号
from score
where 课程号='0004' and 成绩 <60
order by 成绩 desc;
```

### 13、统计每门课程的学生选修人数(超过2人的课程才统计)

### 要求输出课程号和选修人数，查询结果按人数降序排序，若人数相同，按课程号升序排序

```mysql
select 课程号, count(学号) as '选修人数'
from score
group by 课程号
having count(学号)>2
order by count(学号) desc,课程号 asc;
```

### 14、查询两门以上不及格课程的同学的学号及其平均成绩

```mysql
select 学号, avg(成绩) as 平均成绩
from score
where 成绩 <60
group by 学号
having count(课程号)>2;
```

![](http://imgcloud.duiyi.xyz//data20200625200928.png).

### 15、查询学生的总成绩并进行排名

```mysql
select 学号 ,sum(成绩) as 总成绩
from score 
group by 学号
order by sum(成绩);
```

### 16、查询平均成绩大于60分的学生的学号和平均成绩

```mysql
select 学号 ,avg(成绩) 
from score 
group by 学号  
having avg(成绩 )>60;
```



## 三、复杂查询

### 17、查询所有课程成绩小于60分学生的学号、姓名

```mysql
select student.学号,姓名
from student,score
where 成绩<60 and student.`学号`=score.`学号`;
```

### 18、查询没有学全所有课的学生的学号、姓名

```mysql
select score.学号,姓名
from student,score
group by score.学号
having count(课程号)<(select count(课程号) from course);
```

### 19、查询出只选修了两门课程的全部学生的学号和姓名

```mysql
select score.学号,姓名
from student,score
where student.`学号`=score.`学号`
group by score.`学号`
having count(课程号)=2;
```



![](http://imgcloud.duiyi.xyz//data20200626124503.png)

### 20、1990年出生的学生名单

```mysql
select 学号,姓名 
from student 
where year(出生日期)=1990; 
```

### 21、查询各学生的年龄（精确到月份）

```mysql
select 学号,timestampdiff(month ,出生日期 ,now())/12 as 年龄
from student;
```

### 22、查询本月过生日的学生

```mysql
select * 
from student 
where month (出生日期 )=month(now());
```



## 四、多表查询

### 23、查询所有学生的学号、姓名、选课数、总成绩

```mysql
selecta.学号,a.姓名,count(b.课程号) as 选课数,sum(b.成绩) as 总成绩
from student as a left join score as b
on a.学号 = b.学号
group by a.学号;
```

### 24、查询平均成绩大于85的所有学生的学号、姓名和平均成绩

```mysql
select a.学号,a.姓名, avg(b.成绩) as 平均成绩
from student as a left join score as b 
on a.学号 = b.学号
group by a.学号
having avg(b.成绩)>85;
```

### 25、查询学生的选课情况：学号，姓名，课程号，课程名称

```mysql
select a.学号, a.姓名, c.课程号,c.课程名称
from student a 
inner join score b on a.学号=b.学号 
inner join course c on b.课程号=c.课程号;
```

### 26、查询出每门课程的及格人数和不及格人数

```mysql
select 课程号,
sum(case when 成绩>=60 then 1 
	 else 0 
    end) as 及格人数,
sum(case when 成绩 <  60 then 1 
	 else 0 
    end) as 不及格人数
from score
group by 课程号;
```

### 27、使用分段[100-85],[85-70],[70-60],[<60]来统计各科成绩，分别统计：各分数段人数，课程号和课程名称

```mysql
select a.课程号,b.课程名称,
sum(case when 成绩 between 85 and 100 
	 then 1 else 0 end) as '[100-85]',
sum(case when 成绩 >=70 and 成绩<85 
	 then 1 else 0 end) as '[85-70]',
sum(case when 成绩>=60 and 成绩<70  
	 then 1 else 0 end) as '[70-60]',
sum(case when 成绩<60 then 1 else 0 end) as '[<60]'
from score as a right join course as b 
on a.课程号=b.课程号
group by a.课程号,b.课程名称;
```

### 28、查询课程编号为0003且课程成绩在80分以上的学生的学号和姓名

```mysql
select a.学号,a.姓名
from student  as a inner join score as b on a.学号=b.学号
where b.课程号='0003' and b.成绩>80;
```

![](http://imgcloud.duiyi.xyz//data20200626170456.png).

### 29、检索"0001"课程分数小于60，按分数降序排列的学生信息

思路如图：

![](http://imgcloud.duiyi.xyz//data20200626170630.png).

```mysql
select a.*,b.成绩 
from student as a 
inner join score as b 
on a.学号 =b.学号 
where b.成绩 <60 and b.课程号 =01
order by b.成绩 desc;
```

### 30、查询不同老师所教不同课程平均分从高到低显示

【知识点】分组+条件+排序+多表连接，思路如图

![](http://imgcloud.duiyi.xyz//data20200626173019.png).

```mysql
select a.教师号,a.教师姓名,avg(c.成绩) 
from  teacher as a 
inner join course as b 
on a.教师号= b.教师号
inner join score  c on b.课程号= c.课程号
group by a.教师姓名
order by avg(c.成绩) desc;
```

### 31、查询课程名称为"数学"，且分数低于60的学生姓名和分数

【知识点】多表连接，思路如图

![](http://imgcloud.duiyi.xyz//data20200627130614.png)

```mysql
select a.姓名,b.成绩 
from student as a 
inner join score as b 
on a.学号 =b.学号 
inner join course c on b.课程号 =c.课程号 
where b.成绩  <60 and c.课程名称 ='数学';
```

### 32、查询任何一门课程成绩在70分以上的姓名、课程名称和分数（与上题类似）

```mysql
select a.姓名,c.课程名称,b.成绩
from student as a
Inner join score as b
on a.学号=b.学号
Inner join course c on b.课程号=c.课程号
where b.成绩>70;
```

### 33、查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

【知识点】分组+条件+多表连接

翻译成大白话:计算每个学号不及格分数个数，筛选出大于2个的学号并找出姓名，平均成绩，思路如图：

![img](https://pic4.zhimg.com/80/v2-e1e06e7678a85c0b2a0495cd22bea68f_1440w.jpg)

```mysql
select b.姓名,avg(a.成绩),a.学号
from score as a
Inner join student as b
on a.学号=b.学号
where a.成绩<60
group by a.学号
having count(a.学号)>=2;
```

### 34、查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩

![](http://imgcloud.duiyi.xyz//data20200627134606.png)

```mysql
select distinct a.学号,a.成绩,a.课程号
from score as a
inner join score as b
on a.学号 = b.学号
where a.成绩 = b.成绩 and a.课程号 != b.课程号;
```

### 35、查询课程编号为“0001”的课程比“0002”的课程成绩高的所有学生的学号

```mysql
select a.学号  
from 
(select 学号 ,成绩 from score where 课程号=01) as a
inner join 
(select 学号 ,成绩 from score where 课程号=02) as b
on a.学号 =b.学号 
inner join student c on c.学号 =a.学号 
where a.成绩 >b.成绩 ;
```

### 36、查询学过编号为“0001”的课程并且也学过编号为“0002”的课程的学生的学号、姓名

![](http://imgcloud.duiyi.xyz//data20200627141632.png)

```mysql
select a.学号  
from 
(select 学号 ,成绩 from score where 课程号=01) as a
inner join 
(select 学号 ,成绩 from score where 课程号=02) as b
on a.学号 =b.学号 
inner join student c on c.学号 =a.学号 
where a.成绩 >b.成绩 ;
```

### 37、查询学过“孟扎扎”老师所教的所有课的同学的学号、姓名

![](http://imgcloud.duiyi.xyz//data20200627141732.png)

```
select s.学号,s.姓名,a.学号,b.课程号,c.教师号,c.教师姓名
from student as s
inner join score as a
on s.`学号`=a.学号
inner join course b on a.课程号=b.课程号
inner join teacher c on b.教师号= c.教师号
where c.教师姓名 ='孟扎扎';
```

### 38、查询没学过"孟扎扎"老师讲授的任一门课程的学生姓名（与上题类似，"没学过"用not in来实现)

```mysql
select 姓名,学号
from student
WHERE 学号 NOT IN(
select a.学号
from student as a
inner join score AS b
on a.学号 =b.学号
INNER	JOIN course AS c ON b.课程号=c.课程号
INNER	JOIN teacher AS d ON c.教师号=d.教师号
where d.教师姓名 ='孟扎扎');
```

### 39、查询没学过“孟扎扎”老师课的学生的学号、姓名（与上题类似）

```mysql
select 学号, 姓名 
from student
where 学号 not  in
(select 学号 from score where 课程号=
(select 课程号 from course  where 教师号 = 
(select 教师号 from teacher where 教师姓名 ='孟扎扎')
)
);
```

### 40、查询选修“孟扎扎”老师所授课程的学生中成绩最高的学生姓名及其成绩（与上题类似,用成绩排名，用 limit 1得出最高一个）

```mysql
select a.姓名,b.成绩
From student as a
Inner join score as b on a.学号=b.学号
Inner join course as c on b.课程号=c.课程号
Inner join teacher as d on c.教师号=d.教师号
where d.教师姓名='孟扎扎'
order by b.成绩 desc limit 1;
```

### 41、查询至少有一门课与学号为“0001”的学生所学课程相同的学生的学号和姓名

```mysql
select 学号,姓名
from student 
where 学号 in
(select distinct(学号) from score where 课程号 in
(select 课程号 from score where 学号=0001))
and 学号!=0001;
```

### 42、按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

【知识点】多表连接 新建字段 ，思路如图

![](http://imgcloud.duiyi.xyz//data20200627155546.png)

```mysql
select a.学号,avg(a.成绩),
max(case when b.课程名称='数学' then a.成绩 else null end) AS 数学,
max(case when b.课程名称='语文' then a.成绩 else null end) AS 语文,
max(case when b.课程名称='英语' then a.成绩 else null end) AS 英语
from score as a
Inner join course as b
on a.课程号=b.课程号
group by a.学号;
```



## 五、SQL高级功能：窗口函数

![](http://imgcloud.duiyi.xyz//data20200627163812.png)

### 43、查询学生平均成绩及其名次

【知识点】窗口函数排名，思路如图

![](http://imgcloud.duiyi.xyz//data20200627163843.png)

```mysql
select 学号,avg(成绩) as 平均成绩,row_number() over( order by avg(成绩) DESC) AS 名次
from score
group by 学号;
```

### 44、按各科成绩进行排序，并显示排名

![](http://imgcloud.duiyi.xyz//data20200627164203.png)

```mysql
select 课程号,row_number() over(partition by 课程号 order BY 成绩) as 排名
from score;
```

### 45、查询每门功成绩最好的前两名学生姓名

【知识点】窗口函数排名+多表连接+条件

![preview](https://pic1.zhimg.com/v2-cc60234de0d2e1320920cb3e5dd693dc_r.jpg)

```mysql
select a.课程号,b.姓名,a.成绩,a.ranking from (
select 课程号,学号,成绩,row_number() over(partition by 课程号 order by 成绩 desc) as ranking
from  score) as a 
inner join student b on a.学号=b.学号 
where a.ranking<3;
```

### 45、查询所有课程的成绩第2名到第3名的学生信息及该课程成绩（与上一题相似）

```mysql
select b.姓名,a.课程号,a.成绩 
from (
select 课程号,学号,成绩,row_number() over( partition by 课程号 order by 成绩 desc) as ranking
from  score ) as a 
inner join student as b 
on a.学号 =b.学号 
where a.ranking in(2,3);
```

### 46、查询各科成绩前三名的记录（不考虑成绩并列情况）（与上一题相似）

```mysql
select b.姓名,a.课程号,a.成绩 
from(
select 课程号,学号,成绩,
row_number() over( partition by 课程号 order by 成绩 desc) as 'ranking'
from  score) as a 
inner join student as b 
on a.学号=b.学号 
where a.ranking<4;
```

