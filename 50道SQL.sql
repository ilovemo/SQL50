-- 1.查询姓“孟”老师的个数
SELECT COUNT(教师号)
FROM teacher
WHERE 教师姓名 LIKE "孟%";

-- 2.查询课程编号为“0002”的总成绩
SELECT SUM(成绩)
FROM score
WHERE 课程号 = '0002';

-- 3.查询选了课程的学生人数
SELECT COUNT(DISTINCT 学号) AS 学生人数
FROM score;

-- 4.查询各科成绩最高和最低的分， 以如下的形式显示：课程号，最高分，最低分
SELECT 课程号, MAX(成绩) AS 最高分, MIN(成绩) AS 最低分
FROM score
GROUP BY 课程号;

-- 5.查询每门课程被选修的学生数
SELECT 课程号, COUNT(学号) AS 人数
FROM score
GROUP BY 课程号;

-- 6.查询男生、女生人数
SELECT 性别, COUNT(*) AS 人数
FROM student
GROUP BY 性别;

-- 7.查询平均成绩大于60分学生的学号和平均成绩
SELECT 学号, AVG(成绩) AS 平均成绩
FROM score
GROUP BY 学号
HAVING AVG(成绩)>60;

-- 8.查询至少选修两门课程的学生学号
SELECT 学号
FROM score
GROUP BY 学号
HAVING COUNT(课程号)>=2;

-- 9.查询同名同姓学生名单并统计同名人数
SELECT 姓名, COUNT(*) AS 人数
FROM student
GROUP BY 姓名
HAVING COUNT(*)>=2;

-- 10.查询不及格的课程并按课程号从大到小排列
SELECT 课程号
FROM score
WHERE 成绩<60
ORDER BY 课程号 DESC;

-- 11.查询每门课程的平均成绩，结果按平均成绩升序排序，平均成绩相同时，按课程号降序排列
SELECT 课程号, AVG(成绩) AS 平均成绩
FROM score
GROUP BY 课程号
ORDER BY 平均成绩 ASC,课程号 DESC;

-- 12.检索课程编号为“0004”且分数小于60的学生学号，结果按按分数降序排列
SELECT 学号
FROM score
WHERE 课程号='0004' AND 成绩<60
ORDER BY 成绩 DESC;

-- 13.统计每门课程的学生选修人数(超过2人的课程才统计) 
-- 要求输出课程号和选修人数，查询结果按人数降序排序，若人数相同，按课程号升序排序
SELECT 课程号, COUNT(学号) AS 选修人数
FROM score
GROUP BY 课程号
HAVING COUNT(学号)>2
ORDER BY COUNT(学号) DESC,课程号 ASC;

-- 14.查询两门以上不及格课程的同学的学号及其平均成绩
SELECT 学号, AVG(成绩) AS 平均成绩
FROM score
WHERE 成绩<60
GROUP BY 学号
HAVING COUNT(课程号)>2;

-- 15.查询学生的总成绩并进行排名
SELECT 学号, SUM(成绩) AS 总成绩
FROM score
GROUP BY 学号
ORDER BY SUM(成绩) ASC;

-- 16.查询平均成绩大于60分的学生的学号和平均成绩
SELECT 学号, AVG(成绩)
FROM score
GROUP BY 学号
HAVING AVG(成绩)>60;

-- 17.查询所有课程成绩小于60分学生的学号、姓名
SELECT student.学号,姓名
FROM student,score
WHERE 成绩<60 AND student.`学号`=score.`学号`;

-- 18.查询没有学全所有课的学生的学号、姓名
SELECT score.学号,姓名
FROM student,score
GROUP BY score.学号
HAVING COUNT(课程号)<(
SELECT COUNT(课程号)
FROM course);

-- 19.查询出只选修了两门课程的全部学生的学号和姓名
SELECT score.学号,姓名
FROM student,score
WHERE student.`学号`=score.`学号`
GROUP BY score.`学号`
HAVING COUNT(课程号)=2;

-- 20.1990年出生的学生名单
SELECT 学号,姓名
FROM student
WHERE YEAR(出生日期)=1990;

-- 21.查询各学生的年龄（精确到月份）
SELECT 学号, TIMESTAMPDIFF(MONTH,出生日期, NOW())/12 AS 年龄
FROM student;

-- 22.查询本月过生日的学生
SELECT *
FROM student
WHERE MONTH(出生日期) = MONTH(NOW());

-- 23.查询所有学生的学号、姓名、选课数、总成绩
SELECT a.学号,a.姓名, COUNT(b.课程号) AS 选课数, SUM(b.成绩) AS 总成绩
FROM student AS a
LEFT JOIN score AS b ON a.学号 = b.学号
GROUP BY a.学号;

-- 24.查询平均成绩大于85的所有学生的学号、姓名和平均成绩
SELECT a.学号,a.姓名, AVG(b.成绩) AS 平均成绩
FROM student AS a LEFT JOIN score AS b 
ON a.学号 = b.学号
GROUP BY a.学号
HAVING AVG(b.成绩)>85;

-- 25.查询学生的选课情况：学号，姓名，课程号，课程名称
SELECT a.学号, a.姓名, c.课程号,c.课程名称
FROM student a
INNER JOIN score b ON a.学号=b.学号
INNER JOIN course c ON b.课程号=c.课程号;

-- 26.查询出每门课程的及格人数和不及格人数
SELECT 课程号, SUM(CASE WHEN 成绩>=60 THEN 1 ELSE 0 END) AS 及格人数, SUM(CASE WHEN 成绩 < 60 THEN 1 ELSE 0 END) AS 不及格人数
FROM score
GROUP BY 课程号;

-- 27.使用分段[100-85],[85-70],[70-60],[<60]来统计各科成绩，分别统计：各分数段人数，课程号和课程名称
SELECT a.课程号,b.课程名称, SUM(CASE WHEN 成绩 BETWEEN 85 AND 100 THEN 1 ELSE 0 END) AS '[100-85]', SUM(CASE WHEN 成绩 >=70 AND 成绩<85 THEN 1 ELSE 0 END) AS '[85-70]', SUM(CASE WHEN 成绩>=60 AND 成绩<70 THEN 1 ELSE 0 END) AS '[70-60]', SUM(CASE WHEN 成绩<60 THEN 1 ELSE 0 END) AS '[<60]'
FROM score AS a
RIGHT JOIN course AS b ON a.课程号=b.课程号
GROUP BY a.课程号,b.课程名称;

-- 28.查询课程编号为0003且课程成绩在80分以上的学生的学号和姓名
SELECT a.学号,a.姓名
FROM student AS a
INNER JOIN score AS b ON a.学号=b.学号
WHERE b.课程号='0003' AND b.成绩>80;

-- 29.检索"0001"课程分数小于60，按分数降序排列的学生信息
SELECT a.*,b.成绩
FROM student AS a
INNER JOIN score AS b ON a.学号 =b.学号
WHERE b.成绩 <60 AND b.课程号 ='0001'
ORDER BY b.成绩 DESC;

-- 30.查询不同老师所教不同课程平均分从高到低显示
SELECT a.教师号,a.教师姓名, AVG(c.成绩)
FROM teacher AS a
INNER JOIN course AS b ON a.教师号= b.教师号
INNER JOIN score c ON b.课程号= c.课程号
GROUP BY a.教师姓名
ORDER BY AVG(c.成绩) DESC;

-- 31.查询课程名称为"数学"，且分数低于60的学生姓名和分数
SELECT a.姓名,b.成绩
FROM student AS a
INNER JOIN score AS b ON a.学号 =b.学号
INNER JOIN course c ON b.课程号 =c.课程号
WHERE b.成绩 <60 AND c.课程名称 ='数学';

-- 32.查询任何一门课程成绩在70分以上的姓名、课程名称和分数（与上题类似）
SELECT a.姓名,c.课程名称,b.成绩
FROM student AS a
INNER JOIN score AS b ON a.学号=b.学号
INNER JOIN course c ON b.课程号=c.课程号
WHERE b.成绩>70;

-- 33.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩
SELECT b.姓名, AVG(a.成绩),a.学号
FROM score AS a
INNER JOIN student AS b ON a.学号=b.学号
WHERE a.成绩<60
GROUP BY a.学号
HAVING COUNT(a.学号)>=2;

-- 34.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
SELECT DISTINCT a.学号,a.成绩,a.课程号
FROM score AS a
INNER JOIN score AS b
ON a.学号 = b.学号
WHERE a.成绩 = b.成绩 AND a.课程号 != b.课程号;

-- 35.查询课程编号为“0001”的课程比“0002”的课程成绩高的所有学生的学号
SELECT a.学号 
​
FROM 
(
SELECT 学号,成绩
FROM score
WHERE 课程号=01) AS a
INNER JOIN 
(
SELECT 学号,成绩
FROM score
WHERE 课程号=02) AS b ON a.学号 =b.学号
INNER JOIN student c ON c.学号 =a.学号
WHERE a.成绩 >b.成绩 ;

-- 36.查询学过编号为“0001”的课程并且也学过编号为“0002”的课程的学生的学号、姓名
SELECT a.学号 
​​
FROM 
(
SELECT 学号,成绩
FROM score
WHERE 课程号=01) AS a
INNER JOIN 
(
SELECT 学号,成绩
FROM score
WHERE 课程号=02) AS b ON a.学号 =b.学号
INNER JOIN student c ON c.学号 =a.学号
WHERE a.成绩 >b.成绩 ;

-- 37.查询学过“孟扎扎”老师所教的所有课的同学的学号、姓名
SELECT s.学号,s.姓名,a.学号,b.课程号,c.教师号,c.教师姓名
FROM student AS s
INNER JOIN score AS a ON s.`学号`=a.学号
INNER JOIN course b ON a.课程号=b.课程号
INNER JOIN teacher c ON b.教师号= c.教师号
WHERE c.教师姓名 ='孟扎扎';

-- 38.查询没学过"孟扎扎"老师讲授的任一门课程的学生姓名（与上题类似，"没学过"用not in来实现)
SELECT 姓名,学号
FROM student
WHERE 学号 NOT IN(
SELECT a.学号
FROM student AS a
INNER JOIN score AS b ON a.学号 =b.学号
INNER JOIN course AS c ON b.课程号=c.课程号
INNER JOIN teacher AS d ON c.教师号=d.教师号
WHERE d.教师姓名 ='孟扎扎');

-- 39.查询没学过“孟扎扎”老师课的学生的学号、姓名（与上题类似）
SELECT 学号, 姓名
FROM student
WHERE 学号 NOT in
(
SELECT 学号
FROM score
WHERE 课程号=
(
SELECT 课程号
FROM course
WHERE 教师号=
(
SELECT 教师号
FROM teacher
WHERE 教师姓名 ='孟扎扎')
)
);

-- 40.查询选修“孟扎扎”老师所授课程的学生中成绩最高的学生姓名及其成绩（与上题类似,用成绩排名，用 limit 1得出最高一个）
SELECT a.姓名,b.成绩
FROM student AS a
INNER JOIN score AS b ON a.学号=b.学号
INNER JOIN course AS c ON b.课程号=c.课程号
INNER JOIN teacher AS d ON c.教师号=d.教师号
WHERE d.教师姓名='孟扎扎'
ORDER BY b.成绩 DESC
LIMIT 1;

-- 41.查询至少有一门课与学号为“0001”的学生所学课程相同的学生的学号和姓名
SELECT 学号,姓名
FROM student
WHERE 学号 in
(
SELECT DISTINCT(学号)
FROM score
WHERE 课程号 in
(
SELECT 课程号
FROM score
WHERE 学号=0001)) AND 学号!=0001;

-- 42.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
SELECT a.学号, AVG(a.成绩), MAX(CASE WHEN b.课程名称='数学' THEN a.成绩 ELSE NULL END) AS 数学, MAX(CASE WHEN b.课程名称='语文' THEN a.成绩 ELSE NULL END) AS 语文, MAX(CASE WHEN b.课程名称='英语' THEN a.成绩 ELSE NULL END) AS 英语
FROM score AS a
INNER JOIN course AS b ON a.课程号=b.课程号
GROUP BY a.学号;

-- 43.查询学生平均成绩及其名次
SELECT 学号, AVG(成绩) AS 平均成绩,row_number() over(
ORDER BY AVG(成绩) DESC) AS 名次
FROM score
GROUP BY 学号;

-- 44.按各科成绩进行排序，并显示排名
SELECT 课程号,row_number() over(PARTITION BY 课程号
ORDER BY 成绩) AS 排名
FROM score;

-- 45.查询每门功成绩最好的前两名学生姓名
SELECT a.课程号,b.姓名,a.成绩,a.ranking​
FROM (
SELECT 课程号,学号,成绩,
row_number() over(PARTITION BY 课程号
ORDER BY 成绩 DESC) AS ranking​
FROM score) AS a
INNER JOIN student b ON a.学号 =b.学号
WHERE a.ranking<3;

-- 45.查询所有课程的成绩第2名到第3名的学生信息及该课程成绩（与上一题相似）
SELECT b.姓名,a.课程号,a.成绩
FROM (
SELECT 课程号,学号,成绩,row_number() over(PARTITION BY 课程号
ORDER BY 成绩 DESC) AS ranking
FROM score) AS a
INNER JOIN student AS b ON a.学号 =b.学号
WHERE a.ranking in(2,3);

-- 46.查询各科成绩前三名的记录（不考虑成绩并列情况）（与上一题相似）
SELECT b.姓名,a.课程号,a.成绩
FROM(
SELECT 课程号,学号,成绩,
row_number() over(PARTITION BY 课程号
ORDER BY 成绩 DESC) AS 'ranking'
FROM score) AS a
INNER JOIN student AS b ON a.学号=b.学号
WHERE a.ranking<4;